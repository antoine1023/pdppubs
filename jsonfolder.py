#!/usr/bin/env python
#-*- coding:utf-8 -*-

import json
from optparse import OptionParser
import os
import sys

from encoding import Encoder


##############################################################################


LIST_DIR_EXTENSION = ".list"
OBJECT_DIR_EXTENSION = ".object"
DEFAULT_STRING_FILE_EXTENSION = ".md"


##############################################################################


def read_json_file(path):
    with open(path, 'r') as json_file:
        content = json_file.read()
    return json.loads(content)


def read_file(path):
    with open(path, 'r') as f:
        content = f.read()
    return content


##############################################################################


class JSONFolder(object):

    def __init__(self, encoder, verbose=True):
        self.encoder = encoder
        self.verbose = verbose

    #----------------------------------------------------------------- Folding

    def fold(self, dir_path, output):
        """
        Converts a direcory to a new JSON file.

        dir_path: The path of the source diretory
        output: The path of the new JSON file
        """
        path = os.path.normpath(dir_path)
        result = self.fold_file_or_dir(dir_path)
        if result is None:
            if verbose:
                print "Nothing has been folded."
            return
        index, identifier, data = result
        with open(output, 'w') as f:
            f.write(json.dumps(data, indent=4))

    def fold_file_or_dir(self, path):
        path = os.path.normpath(path)
        """
        Returns a tuple (potential_index, potential_identifier, data) or
        None if nothing has been folded.
        """
        if os.path.isdir(path):
            return self.fold_dir(path)
        else:
            return self.fold_file(path)

    def fold_dir(self, dir_path):
        """
        Returns a tuple (potential_index, potential_identifier, data)
        or None if the directory has no extension.
        """
        dir_name = os.path.basename(dir_path)
        dir_name_base, ext = os.path.splitext(dir_name)
        potential_index, potential_id = self.get_potential_index_and_id(
            dir_name_base)

        if ext == LIST_DIR_EXTENSION:
            data = self.fold_list_dir(dir_path)
        elif ext == OBJECT_DIR_EXTENSION:
            data = self.fold_dict_dir(dir_path)
        else:
            if self.verbose:
                print "{} has no extension, it is ignored".format(dir_path)
            return None

        return potential_index, potential_id, data

    def fold_list_dir(self, dir_path):
        children = []

        for child_name in os.listdir(dir_path):
            path = os.path.join(dir_path, child_name)
            result = self.fold_file_or_dir(path)
            if result is None:
                continue
            index, identifier, data = result
            if index is not None:
                children.append((index, data))
            else:
                print 'Warning: {} has no valid index'.format(path)

        children.sort(key=lambda d: d[0])       # sort by index

        return [data for index, data in children]

    def fold_dict_dir(self, dir_path):
        children = {}

        for child_name in os.listdir(dir_path):
            path = os.path.join(dir_path, child_name)
            result = self.fold_file_or_dir(path)
            if result is None:
                continue
            index, identifier, data = result
            if len(identifier) == 0:
                print 'Warning: {} has no valid identifier'.format(path)
            if identifier in children:
                print 'Warning: {} redefines {}'.format(path, identifier)
            else:
                children[identifier] = data

        return children

    def fold_file(self, file_path):
        """
        Returns a tuple: (potential_index, potential_identifier, data)
        """
        file_name = os.path.basename(file_path)
        file_name_base, ext = os.path.splitext(file_name)
        potential_index, potential_id = self.get_potential_index_and_id(
            file_name_base)

        if ext == '.json':
            data = read_json_file(file_path)
        else:
            data = read_file(file_path)

        return potential_index, potential_id, data

    def get_potential_index_and_id(self, name_without_ext):
        """
        name: The name of a file or direcory, without extension
        Returns a tuple (potential_index, potential_identifier)
        """
        return (
            self._get_potential_index(name_without_ext),
            self.encoder.decode(name_without_ext)
        )

    def _get_potential_index(self, name_without_ext):
        """
        name: The name of a file or direcory, without extension
        """
        try:
            return int(name_without_ext)
        except ValueError:
            return ord(name_without_ext[0])

    #--------------------------------------------------------------- Unfolding

    def unfold(self, file_path, output):
        """
        Converts a JSON file to a new directory.

        file_path: The path of the source JSON file
        output: The path of the new direcory
        """
        path = os.path.normpath(file_path)

        # file name without extension
        file_name = os.path.splitext(os.path.basename(file_path))[0]

        # os.mkdir(output)
        json_data = read_json_file(file_path)
        self.unfold_object(os.curdir, output, json_data)

    def unfold_object(self, dir_path, identifier, o):
        """
        identifier: The base name of the new file or directory,
        without extension.
        o: Any foldable Python object
        """
        if isinstance(o, list):
            self.unfold_list(dir_path, identifier, o)

        elif isinstance(o, dict):
            self.unfold_dict(dir_path, identifier, o)

        elif isinstance(o, str):
            file_path = os.path.join(
                dir_path, identifier + DEFAULT_STRING_FILE_EXTENSION)
            with open(file_path, 'w') as f:
                f.write(o)

        elif isinstance(o, unicode):
            file_path = os.path.join(
                dir_path, identifier + DEFAULT_STRING_FILE_EXTENSION)
            with open(file_path, 'w') as f:
                f.write(o.encode('utf-8'))

        else:
            file_path = os.path.join(dir_path, identifier + '.json')
            with open(file_path, 'w') as f:
                f.write(json.dumps(o))

    def unfold_list(self, parent_dir_path, identifier, l):
        dir_path = os.path.join(
            parent_dir_path, identifier + LIST_DIR_EXTENSION)
        os.mkdir(dir_path)
        i = 0
        while i < len(l):
            self.unfold_object(dir_path, str(i + 1), l[i])
            i += 1

    def unfold_dict(self, parent_dir_path, identifier, d):
        dir_path = os.path.join(
            parent_dir_path, identifier + OBJECT_DIR_EXTENSION)
        os.mkdir(dir_path)
        for key, value in d.items():
            self.unfold_object(dir_path, self.encoder.encode(key), value)


##############################################################################


def main():

    parser = OptionParser()

    parser.usage = '%prog SOURCE'

    parser.description = 'Converts Progdupeupl tutorials from '\
        'directory trees to JSON files and vice versa.'

    parser.add_option("-v", "--verbose",
                      action="store_true", dest="verbose", default=True,
                      help="make lots of noise [default]")

    parser.add_option('-o', '--output', default='a.out',
                      metavar='OUTPUT', help='write output to OUTPUT')

    options, args = parser.parse_args()

    if len(args) == 0:
        print "No input file or directory. "\
            "Rerun with '--help' for more information."
        return 0

    arg = args[0]
    if not os.access(arg, os.R_OK):
        print "Cannot access to {}.".format(arg)
        return 0

    verbose = options.verbose
    output = options.output

    encoder = Encoder('#', Encoder.is_valid_char_strict)
    folder = JSONFolder(encoder, verbose)

    if os.path.isfile(arg):
        folder.unfold(arg, output)
    else:
        folder.fold(arg, output)
    return 0


##############################################################################


if __name__ == '__main__':
    sys.exit(main())
