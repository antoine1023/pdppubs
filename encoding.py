#!/usr/bin/env python
#-*- coding:utf-8 -*-

import string
import unittest


class Encoder(object):
    """
    A little encoder for file names.
    Actually, supports only 0000–​FFFF unicode range.
    """

    def __init__(self, escape_character, is_encoding_valid_char):
        """
        is_encoding_valid_chars: A function which tests if a character must
        be escaped.
        """
        self.escape_character = escape_character
        self.is_encoding_valid_char = is_encoding_valid_char

    def _escape(self, char):
        cp = ord(char)
        if (cp > 0xffff):
            raise Exception('Not implemented')
        return self.escape_character + format(cp, 'x').zfill(4)

    def encode(self, unicode_string):
        if not isinstance(unicode_string, unicode):
            raise TypeError()
        r = ''
        for c in unicode_string:
            if c == self.escape_character:
                r += self._escape(c)
            elif self.is_encoding_valid_char(c):
                r += c.encode("utf-8")
            else:
                r += self._escape(c)
        return r

    def decode(self, original_string):
        string = original_string
        if not isinstance(string, str):
            raise TypeError()

        string = unicode(string, "utf-8")

        r = u''
        while len(string) > 0:
            c = string[0]
            string = string[1:]
            if c == self.escape_character:
                if len(string) < 4:
                    raise Exception("Cannot decode {!r}".format(
                        original_string))
                code_point_string = string[:4]
                string = string[4:]
                try:
                    code_point = int(code_point_string, 16)
                except ValueError:
                    raise Exception(
                        "Cannot decode {!r}, {} is not a valid code "
                        "point.".format(original_string, code_point_string)
                        )
                r += unichr(code_point)
            else:
                r += c
        return r

    @staticmethod
    def is_valid_char_strict(c):
        return c in string.ascii_letters + string.digits + '._-'

    @staticmethod
    def is_valid_char_extended(c):
        if Encoder.is_valid_char_strict(c):
            return True
        else:
            return ord(c) > 127


class TestEncoder(unittest.TestCase):

    def setUp(self):
        self.source = u"Bienvenue sur Progdupeupl,_une_communauté" \
            u"_de_passionnés_d'informatique."

    def test_extended_encoding(self):
        self._test_encoder(Encoder('@', Encoder.is_valid_char_extended))
        self._test_encoder(Encoder(' ', Encoder.is_valid_char_extended))

    def test_strict_encoding(self):
        self._test_encoder(Encoder('#', Encoder.is_valid_char_strict))
        self._test_encoder(Encoder('u', Encoder.is_valid_char_strict))

    def _test_encoder(self, encoder):
        encoded = encoder.encode(self.source)
        self.assertEqual(encoder.decode(encoded), self.source)


if __name__ == '__main__':
    unittest.main()
