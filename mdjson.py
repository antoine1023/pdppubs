#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
A little markdown analyser.
"""

import argparse
import json
import os
import pprint
import sys

##############################################################################


class Position (object):
    """
    Represents a position in a text file.

    >>> p = Position(12, 102)
    >>> p
    <Position line=12 index=102>
    >>> p.line
    12
    >>> p.index
    102
    """

    def __init__(self, line, index):
        self.line = line
        self.index = index

    def __repr__(self):
        return "<Position line={} index={}>".format(self.line, self.index)

    def __str__(self):
        return "(line {}; index {})".format(self.line, self.index)

##############################################################################


class StringReader (object):
    """
    Reads a string character after character.

    >>> r = StringReader("hello\\nWorld!")
    >>> r.next()
    'h'
    >>> r.next()
    'e'
    >>> r.back()
    >>> r.back()
    >>> r.next()
    'h'
    >>> r.next()
    'e'
    >>> r.skip("ol")
    'llo'
    >>> r.has_more()
    True
    >>> r.next()
    '\\n'
    >>> r.line
    2
    >>> r.back()
    >>> r.line
    1
    >>> r.next()
    '\\n'
    >>> r.line
    2
    >>> r.position
    <Position line=2 index=6>
    >>> r.match("test")
    False
    >>> r.position
    <Position line=2 index=6>
    >>> r.match("World!")
    True
    >>> r.has_more()
    False
    """

    def __init__(self, string):
        self._string = string
        self.index = 0
        self.line = 1

    def has_more(self):
        return self.index != len(self._string)

    def next(self):
        if self.index == len(self._string):
            raise Exception("No more characters")
        c = self._string[self.index]
        if c == '\n':
            self.line += 1
        self.index += 1
        return c

    def skip(self, skippables):
        """
        skippables: A string of all the skippables characters
        Returns the skipped characters
        """
        skipped = ""
        while self.has_more():
            c = self.next()
            if not c in skippables:
                self.back()
                break
            skipped += c
        return skipped

    def back(self):
        if self.index == 0:
            raise Exception("Cannot go back")
        self.index -= 1
        c = self._string[self.index]
        if c == '\n':
            self.line -= 1

    def match(self, string):
        begin_index = self.index
        begin_line = self.line
        while self.has_more:
            c = self.next()
            if c != string[0]:
                break
            string = string[1:]
            if len(string) == 0:
                return True
        self.index = begin_index
        self.line = begin_line
        return False

    @property
    def position(self):
        return Position(self.line, self.index)

    @position.setter
    def position(self, new_value):
        self.line = new_value.line
        self.index = new_value.index

##############################################################################


class Token (object):

    def __init__(self, string, position):
        if not isinstance(string, unicode):
            raise Exception()
        self._string = string
        self._position = position

    @property
    def position(self):
        return self._position

    @property
    def string(self):
        return self._string

    def __repr__(self):
        pass


class TokenReader (object):
    """
    Reads a list of tokens.
    """

    def __init__(self, list):
        self._list = list
        self.index = 0

    def has_more(self):
        return self.index != len(self._list)

    def next(self):
        if self.index == len(self._list):
            raise Exception("No more tokens")
        c = self._list[self.index]
        self.index += 1
        return c

    def back(self):
        if self.index == 0:
            raise Exception("Cannot go back")
        self.index -= 1
        c = self._list[self.index]


##############################################################################


class HeadingToken (Token):
    def __init__(self, string, position, level):
        super(HeadingToken, self).__init__(string, position)
        self._level = level

    def __repr__(self):
        return '<HeadingToken string={!r} position={} level={}>'.format(
            self._string.encode("utf-8"),
            self._position,
            self._level,
        )

    @property
    def level(self):
        return self._level


class TextToken (Token):
    def __init__(self, string, position):
        super(TextToken, self).__init__(string, position)

    def __repr__(self):
        return '<TextToken string={!r} position={}>'.format(
            self._string.encode("utf-8"),
            self._position,
        )


class LineToken (Token):
    def __init__(self, string, position):
        super(LineToken, self).__init__(string, position)

    def __repr__(self):
        return '<LineToken string={!r} position={}>'.format(
            self._string.encode("utf-8"),
            self._position,
        )


class CodeToken (Token):
    def __init__(self, string, position):
        super(CodeToken, self).__init__(string, position)

    def __repr__(self):
        return '<CodeToken string={!r} position={}>'.format(
            self._string.encode("utf-8"),
            self._position,
        )


class MarkdownLexer (object):
    """
    A simple Markdown lexer.

    It is not very beautiful, but it works.
    """

    def __init__(self, string):
        if not isinstance(string, unicode):
            raise Exception("string should be an unicode")
        self.reader = StringReader(string)
        self.tokens = []
        self.text = u""
        self.text_position = self.reader.position
        self._lex()

    def _read_until(self, target, returns_none_if_not_foud=True):
        """
        Eats characters until target is found.
        If target is not found and returns_none_if_not_foud is True,
        returns None and restores the position of the reader.
        Otherwise, returns the eaten characters.
        """

        eaten_characters = ""
        target_remaining_chars = target
        reader = self.reader
        begin_pos = reader.position
        while reader.has_more():
            c = reader.next()
            eaten_characters += c
            if c == target_remaining_chars[0]:
                target_remaining_chars = target_remaining_chars[1:]
                if len(target_remaining_chars) == 0:
                    return eaten_characters
            else:
                target_remaining_chars = target
        if returns_none_if_not_foud:
            reader.position = begin_pos
            return None
        else:
            return eaten_characters

    def _push_text_token(self):
        if len(self.text) == 0:
            return
        token = TextToken(self.text, self.text_position)
        self.tokens.append(token)
        self.text = u""
        self.text_position = self.reader.position

    def _lex(self):
        reader = self.reader
        self.text_position = reader.position

        while reader.has_more():
            char_position = reader.position

            comment_token = self._lex_comments()
            if comment_token is not None:
                self._push_text_token()
                self.tokens.append(comment_token)
                continue

            c = reader.next()

            if c == "\n" and reader.has_more():
                reader.position = char_position
                t = self._lex_headings()
                if t is None:
                    reader.next()
                    self.text += c
                else:
                    self._push_text_token()
                    self.tokens.append(t)

            if c == "`" and reader.has_more():
                reader.position = char_position
                t = self._lex_code()
                if t is None:
                    reader.next()
                    self.text += c
                else:
                    self._push_text_token()
                    self.tokens.append(t)
            else:
                if c == u'\\' and reader.has_more():
                    c = reader.next()
                self.text += c
        self._push_text_token()

    def _lex_code(self):
        reader = self.reader
        begin_pos = reader.position
        c = reader.next()
        if c == '`':
            backtick_count = 1
            while reader.has_more():
                c = reader.next()
                if c != '`':
                    eaten = self._read_until("`" * backtick_count)
                    if eaten is None:
                        break
                    eaten = c + eaten
                    return CodeToken("`" * backtick_count + eaten, begin_pos)
                backtick_count += 1
        reader.position = begin_pos
        return None

    def _lex_comments(self):
        reader = self.reader
        begin_pos = reader.position
        if reader.match("<!--"):
            eaten = self._read_until("-->")
            if eaten is not None:
                return TextToken("<!--" + eaten, begin_pos)
        reader.position = begin_pos
        return None

    def _lex_headings(self):
        reader = self.reader
        begin_pos = reader.position
        if reader.match('\n') and reader.match('#'):
            heading_level = 1
            while reader.has_more():
                c = reader.next()
                if c != '#':
                    string = self._read_until("\n", False)
                    reader.back()   # A line feed has been eaten
                    string = heading_level * "#" + c + string
                    return HeadingToken(string, begin_pos, heading_level)
                heading_level += 1
            raise Exception("Expected heading at {}".format(
                reader.position))
        reader.position = begin_pos
        return None


##############################################################################


class Section (object):
    """
    Represents a section in a PublicationModel
    """

    def __init__(self, name, heading_level):
        self._heading_level = heading_level
        self._name = name

    @property
    def name(self):
        return self._name

    @property
    def heading_level(self):
        return self._heading_level

    @staticmethod
    def from_json(json_string):
        pass    # TODO

    def __repr__(self):
        return "<Section name={!r} heading_level={}>".format(
            self.name, self.heading_level,
        )


class PublicationModel (object):
    def __init__(self, name, sections):
        self._name = name
        self._sections = sections

    @property
    def name(self):
        return name

    def section_from_heading_level(self, heading_level):
        """
        Search a section from a heading level.
        Returns None if not found.
        """
        for d in self._sections:
            if d.heading_level == heading_level:
                return d
        return None

    def __repr__(self):
        return "<PublicationModel name={}>".format(self.name)


##############################################################################


def markdown_lex(string):
    """
    Returns a token list.
    """
    lexer = MarkdownLexer(string)
    return lexer.tokens


def parse_markdown(publication_model, string):
    """
    Returns a tree.
    """
    tokens = markdown_lex(string)
    reader = TokenReader(tokens)
    nodes = []
    read_until(reader, HeadingToken)
    reader.back()
    while reader.has_more():
        nodes.append(parse_section(publication_model, reader))
    return nodes


def get_heading_name(heading_string):
    """
    Removes trailing and leading sharps and spaces
    """
    return heading_string.strip().strip("#").strip()


def parse_section(publication_model, reader):
    """
    #level: the title level, 1-based.
    reader: a TokenReader.
    """

    heading_token = reader.next()
    heading_name = get_heading_name(heading_token.string)

    introduction = ""
    conclusion = ""
    text = ""

    children = []
    while reader.has_more():
        token = reader.next()
        if isinstance(token, HeadingToken):
            if token.level <= heading_token.level:
                reader.back()
                break
            else:
                name = get_heading_name(token.string)
                if name.lower() == "introduction":
                    introduction = parse_text(reader)
                elif name.lower() == "conclusion":
                    conclusion = parse_text(reader)
                else:
                    reader.back()
                    child = parse_section(publication_model, reader)
                    children.append(child)
        elif isinstance(token, TextToken):
            reader.back()
            text += parse_text(reader)
        else:
            raise Exception()

    section = publication_model.section_from_heading_level(
        heading_token.level
        )

    if section is None:
        children_key_name = "children"
    else:
        children_key_name = section.name
    d = {
        "title": heading_name,
    }
    if len(introduction) != 0:
        d["introduction"] = introduction
    if len(conclusion) != 0:
        d["conclusion"] = conclusion
    if len(text) != 0:
        d["text"] = text
    if len(children) != 0:
        d[children_key_name] = children
    return d


def read_until(reader, token_class):
    while reader.has_more():
        t = reader.next()
        if isinstance(t, token_class):
            return t
    raise Exception("Expected {}.".format(type(token)))


def parse_text(reader):
    """
    Parses a piece of text without headings.
    """
    text = ""
    while reader.has_more():
        token = reader.next()
        if isinstance(token, HeadingToken):
            reader.back()
            break
        else:
            text += token.string
    return text


##############################################################################


def heading_to_markdown(level, name):
    if level < 1:
        raise ValueError()
    s = ("#" * level) + " " + name + " " + ("#" * level)
    if level == 1:
        margin = 79 - len(s)
        s += "#" * margin
    return "\n" + s + "\n"


def json_to_markdown(json_data, level, publication_model):
    section = publication_model.section_from_heading_level(level)

    if isinstance(json_data, list):
        s = ""
        for i in json_data:
            s += json_to_markdown(i, level, publication_model)
        return s

    elif isinstance(json_data, dict):
        s = ""
        if not "title" in json_data:
            raise Exception("There is no title in {}".format(json_data))
        title = json_data["title"]
        s += heading_to_markdown(level, title)

        if "introduction" in json_data:
            s += heading_to_markdown(level + 1, "Introduction")
            s += "\n" + json_data["introduction"] + "\n"

        if "text" in json_data:
            s += json_data["text"]
        elif section.name in json_data:
            d = json_data[section.name]
            s += json_to_markdown(d, level + 1, publication_model)

        if "conclusion" in json_data:
            s += heading_to_markdown(level + 1, "Conclusion")
            s += "\n" + json_data["conclusion"] + "\n"

        return s

    elif isinstance(json_data, str):
        return json_data

    else:
        raise Exception("Cannot convert {} to Markdown".format(json_data))


PUBLICATION_MODELS = {
    "tutorial": PublicationModel(
        "tutorial",
        [
            Section("parts", 0),
            Section("chapters", 1),
            Section("extracts", 2),
        ]
    ),

    "chapters_tutorial": PublicationModel(
        "chapters_tutorial",
        [
            Section("chapters", 0),
            Section("extracts", 1),
        ]
    ),

    "extracts_tutorial": PublicationModel(
        "extracts_tutorial",
        [
            Section("extracts", 0),
        ]
    ),
}

##############################################################################


test_md_to_json_str = u"""
# Partie 1 : Les choses basiques
## Introduction
Partie 1 introduction
## Chapitre 1
### Introduction
Partie 1 chapitre 1 introduction
### Extrait 1
Partie 1 chapitre 1 extrait 1
### Extrait 2
Partie 1 chapitre 1 extrait 2
## Conclusion
Partie 1 conclusion
## Chapitre 1
Partie 1 chap 1
# Partie 2 : Les choses avancées
# Partie 3 : Les choses tordues
```
# Ceci n'est pas un titre !!!
```
<!--
# Ni ceci.
-->
    # Ni cela d'ailleurs.
"""


def test_markdown_to_json():
    data = parse_markdown(PUBLICATION_MODELS["tutorial"], test_md_to_json_str)
    print json.dumps(data, indent=4)


def test_json_to_markdown():
    s = """
    [
        {
            "title": "Partie 1 : Les choses basiques",
            "introduction": "Partie 1 introduction",
            "conclusion": "Partie 1 conclusion"
        },
        {
            "title": "Partie 2 : Les choses avancées",
            "introduction": "Partie 2 introduction",
            "conclusion": "Partie 2 conclusion",
            "chapters": [
                {
                    "title": "Partie 2 chapitre 1",
                    "introduction": "Partie 2 chapitre 1 introduction",
                    "introduction": "Partie 2 chapitre 1 conclusion"
                }
            ]
        }
    ]
    """
    print json_to_markdown(json.loads(s), 1,
                           PUBLICATION_MODELS["tutorial_model"])


def read_file(path):
    with open(path, 'r') as f:
        content = f.read()
    return content


##############################################################################


def main():
    import doctest
    doctest.testmod()

    parser = argparse.ArgumentParser(
        description='Converts Progdupeupl tutorials from Markdown to '
        'JSON and vice versa.'
    )

    parser.add_argument(
        '-o',
        '--output',
        default='a.out',
        metavar='OUTPUT',
        help='write output to OUTPUT'
    )

    parser.add_argument(
        "source",
        metavar='SOURCE',
        help='The source file'
    )

    parser.add_argument(
        'publication_model',
        choices=PUBLICATION_MODELS.keys(),
        help='write output to OUTPUT'
    )

    options = parser.parse_args()
    output_path = options.output
    source_path = options.source
    publication_model = PUBLICATION_MODELS[options.publication_model]

    if not os.access(source_path, os.R_OK):
        print "Cannot access to {}.".format(source_path)
        return 0

    source_text = read_file(source_path)

    source_root, source_ext = os.path.splitext(source_path)
    if source_ext == ".md":
        data = parse_markdown(publication_model, source_text.decode("utf-8"))
        with open(output_path, 'w') as f:
            f.write(json.dumps(data, indent=4))
    elif source_ext == ".json":
        md = json_to_markdown(
            json.loads(source_text),
            1,
            publication_model,
        )
        with open(output_path, 'w') as f:
            f.write(md.encode("utf-8"))
    else:
        print "Unknown extension {}.".format(source_ext)
        return 0


##############################################################################


if __name__ == '__main__':
    # sys.exit(test_markdown_to_json())
    sys.exit(main())
